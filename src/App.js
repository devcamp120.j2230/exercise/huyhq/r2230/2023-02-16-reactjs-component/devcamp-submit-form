import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import TittleComponent from './title/TittleComponent ';
import FormComponent from './content/FormComponent';

function App() {
  return (
    <div className="container bg-light">
      <TittleComponent />
      <FormComponent />
    </div>
  );
}

export default App;
