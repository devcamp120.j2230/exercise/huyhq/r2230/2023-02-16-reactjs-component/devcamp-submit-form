import { Component } from "react";

class FormComponent extends Component {
    onInputChange(e){
        console.log(e.target.value);
    };

    onButtonClick(){
        console.log("Form đã được submit");
    };

    render() {
        return (
            <div className="row form-group p-5">
                <div className="row col-12">
                    <label className='col-4 form-label'>Firstname</label>
                    <div className='col-8'>
                        <input type="text" className="form-control" placeholder='Firstname...' onChange={this.onInputChange}/>
                    </div>
                </div>
                <div className="row col-12">
                    <label className='col-4 form-label'>Lastname</label>
                    <div className='col-8'>
                        <input type="text" className="form-control" placeholder='Lastname...' onChange={this.onInputChange}/>
                    </div>
                </div>
                <div className="row col-12">
                    <label className='col-4 form-label'>Country</label>
                    <div className='col-8' onChange={this.onInputChange}>
                        <select className='form-control'>
                            <option value="vietnam">Viet Nam</option>
                            <option value="canada">Canada</option>
                            <option value="us">My</option>
                        </select>
                    </div>
                </div>
                <div className="row col-12">
                    <label className='col-4 form-label'>Object</label>
                    <div className='col-8'>
                        <textarea className="form-control" placeholder='Write here...' rows="5" onChange={this.onInputChange}/>
                    </div>
                </div>
                <div className="row col-12">
                    <button className='btn btn-success col-1' onClick={this.onButtonClick}>Send</button>
                </div>
            </div>
        )
    }
};

export default FormComponent;